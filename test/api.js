const test = require('blue-tape')
const { join } = require('path')
const { promises: { copyFile, readFile, writeFile } } = require('fs')
const { withFile, withDir } = require('tmp-promise')
const { short } = require('..')

{
  const scenarios = {
    'as plain text': {
      name: 'foo'
    },
    'as non-ASCII Unicode emoji': {
      name: '💩'
    }
  }
  for (const [scenario, given] of Object.entries(scenarios)) {
    test(`Specify shortname ${scenario}`, async (t) => {
      const actual = await short({
        url: 'https://example.com',
        ...given
      })
      t.ok(actual.redirect.from.includes(
        encodeURIComponent(given.name)
      ))
    })
  }
}

{
  const scenarios = {
    'as absolute URL': {
      url: 'https://example.com'
    },
    'as relative URL': {
      url: '/example'
    }
  }
  for (const [scenario, given] of Object.entries(scenarios)) {
    test(`Specify URL ${scenario}`, async (t) => {
      const actual = await short({
        decimal: true,
        ...given
      })
      t.ok(actual.redirect.to.includes(
        encodeURIComponent(given.url)
      ))
    })
  }
}

test('Omit shortening method', async (t) => {
  const given = {
    url: 'https://example.com'
  }
  const actual = short(given)
  t.shouldFail(actual, /No shortening method specified./)
})

test('Omit URL', async (t) => {
  const given = {
    name: 'foo'
  }
  const actual = short(given)
  await t.shouldFail(actual, /Missing a URL to be shortened./)
})

test('Specify configuration object', async (t) => {
  const given = {
    configuration: {},
    url: 'https://example.com',
    name: 'foo'
  }
  const actual = await short(given)
  t.is(actual.configuration, given.configuration)
})

test('Specify configuration file as path or file descriptor', async (t) => {
  await withFile(async (file) => {
    const fixture = {
      hosts: [
        {
          domain: 'example.com',
          redirects: [
            {
              from: '/foo',
              to: '/bar'
            }
          ]
        }
      ]
    }
    await writeFile(file.path, JSON.stringify(fixture))
    const given = {
      file: file.path,
      url: 'https://example.net',
      name: '12345'
    }
    await short(given)
    const actual = JSON.parse(await readFile(file.path))
    t.is(actual.hosts[0].redirects.length, 2)
    t.ok(actual.hosts[0].redirects[1].from.includes(
      encodeURIComponent(given.name)
    ))
    t.ok(actual.hosts[0].redirects[1].to.includes(
      encodeURIComponent(given.url)
    ))
  })
})

{
  const fixtures = join(__dirname, 'fixtures')
  const given = {
    name: 'foo',
    url: 'https://example.com'
  }

  test('Fail with missing configuration file', async (t) => {
    await t.shouldFail(
      short({
        ...given,
        cwd: join(fixtures, 'configuration-missing')
      }),
      /No configuration file found./
    )
  })

  test('Fail with empty configuration file', async (t) => {
    await t.shouldFail(
      short({
        ...given,
        cwd: join(fixtures, 'configuration-empty')
      }),
      /Configuration file is empty./
    )
  })

  test('Fail if configuration is not valid JSON', async (t) => {
    await t.shouldFail(
      short({
        ...given,
        cwd: join(fixtures, 'configuration-invalid-json')
      }),
      /Unexpected token/
    )
  })

  test('Fail if configuration does not have hosts Array', async (t) => {
    await t.shouldFail(
      short({
        ...given,
        cwd: join(fixtures, 'configuration-invalid-hosts')
      }),
      /Invalid configuration: Missing `hosts` array./
    )
  })

  test('Load configuration from current working directory', async (t) => {
    await withDir(async ({ path: directory }) => {
      const fixture = join(fixtures, 'configuration-existant/commonshost.json')
      const temporary = join(directory, 'commonshost.json')
      await copyFile(fixture, temporary)
      const given = {
        cwd: directory,
        name: 'foo',
        url: 'https://example.com'
      }
      await short(given)
      const actual = JSON.parse(await readFile(temporary, 'utf8'))
      t.ok(actual.hosts[0].redirects[0].from.includes(
        encodeURIComponent(given.name)
      ))
      t.ok(actual.hosts[0].redirects[0].to.includes(
        encodeURIComponent(given.url)
      ))
    }, { unsafeCleanup: true })
  })
}
