const test = require('blue-tape')
const { join, resolve } = require('path')
const { promises: { readFile, writeFile } } = require('fs')
const { withDir } = require('tmp-promise')
const { spawnSync } = require('child_process')

test('', async (t) => {
  await withDir(async ({ path: directory }) => {
    const configuration = join(directory, 'commonshost.json')
    const fixture = {
      hosts: [
        {
          redirects: [
            {
              from: '/foo',
              to: '/bar'
            }
          ]
        }
      ]
    }
    await writeFile(configuration, JSON.stringify(fixture))
    const pkg = require(join(__dirname, '../package.json'))
    const short = resolve(__dirname, '..', pkg.bin.short)
    const given = 'https://example.com'
    spawnSync('node', [short, given], { cwd: directory })
    const actual = JSON.parse(await readFile(configuration, 'utf8'))
    t.is(actual.hosts[0].redirects.length, 2)
    t.ok(actual.hosts[0].redirects[1].to.includes(
      encodeURIComponent(given)
    ))
  }, { unsafeCleanup: true })
})
