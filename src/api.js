const { random: randomEmoji } = require('random-unicode-emoji')
const { randomFillSync } = require('crypto')
const { promises: { readFile, writeFile } } = require('fs')
const { findFile } = require('@commonshost/configuration')
const UriTemplate = require('uri-templates')

async function loadConfiguration (file) {
  const raw = await readFile(file, 'utf8')
  if (!raw.length) {
    throw new Error('Configuration file is empty.')
  }
  const configuration = raw.length ? JSON.parse(raw) : {}
  return configuration
}

module.exports.short =
async function short (options) {
  let configuration
  if (options.configuration) {
    configuration = options.configuration
  } else if (options.file) {
    configuration = await loadConfiguration(options.file)
  } else if (options.cwd) {
    const file = await findFile(options.cwd)
    if (!file) {
      throw new Error('No configuration file found.')
    }
    options.file = file
    configuration = await loadConfiguration(file)
  } else {
    configuration = {}
  }

  let name
  if (options.name) {
    name = options.name
  } else if (options.emoji) {
    name = randomEmoji({ count: 2 }).join('')
  } else if (options.hex) {
    const buffer = Buffer.alloc(2)
    name = randomFillSync(buffer).toString('hex')
  } else if (options.decimal) {
    const number = Math.floor(Math.random() * Number.MAX_SAFE_INTEGER)
    name = String(number).substring(0, 4)
  } else {
    throw new Error('No shortening method specified.')
  }

  if (typeof configuration !== 'object') {
    throw new Error('Invalid configuration: Not an object.')
  }
  if (!('hosts' in configuration)) {
    configuration.hosts = []
  }
  if (!Array.isArray(configuration.hosts)) {
    throw new Error('Invalid configuration: Missing `hosts` array.')
  }
  if (configuration.hosts.length === 0) {
    configuration.hosts.push({})
  }

  const host = configuration.hosts[0]
  if (!host.redirects) {
    host.redirects = []
  }

  if (!options.url) {
    throw new Error('Missing a URL to be shortened.')
  }
  const from = new UriTemplate('/{name}').fill({ name })
  const to = new UriTemplate('/redirect/{?url}').fill({ url: options.url })
  const redirect = { from, to }
  host.redirects.push(redirect)

  if (options.file) {
    const json = JSON.stringify(configuration, null, 2)
    await writeFile(options.file, json + '\n')
  }

  return { redirect, configuration, host, file: options.file }
}
