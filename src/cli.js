#!/usr/bin/env node

const argv = process.argv.slice(2)
const isUrl = /^https?:\/\/.+/
const options = {
  version: argv.includes('-v') || argv.includes('--version'),
  help: argv.includes('-h') || argv.includes('--help'),
  decimal: argv.includes('-d') || argv.includes('--decimal') || true,
  emoji: argv.includes('-e') || argv.includes('--emoji'),
  hex: argv.includes('-x') || argv.includes('--hex'),
  url: argv.find((input) => isUrl.test(input)),
  name: argv.find((input) => !isUrl.test(input) && !input.startsWith('-'))
}

if (options.version) {
  require('./commands/version')
} else if (options.help) {
  require('./commands/help')
} else if (!options.url) {
  require('./commands/missingUrl')
} else {
  require('./commands/shorten')(options)
}
