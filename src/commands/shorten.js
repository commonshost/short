const { promises: { readFile } } = require('fs')
const { domainToUnicode } = require('url')
const { short } = require('../..')

module.exports = async function shorten (options) {
  try {
    const { host, redirect } = await short({ cwd: process.cwd(), ...options })
    let domain
    if (host.domain) {
      domain = host.domain
    } else {
      try {
        domain = (await readFile('CNAME', 'utf8'))
          .split('\n')[0].trim()
      } catch (error) {}
    }

    const origin = domain ? `https://${domainToUnicode(domain)}` : ''
    console.log(`🔗 ${origin}${decodeURI(redirect.from)}`)
  } catch (error) {
    console.error(error)
    process.exit(1)
  }
}
